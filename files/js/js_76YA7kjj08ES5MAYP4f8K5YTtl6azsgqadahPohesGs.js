(function ($, root, undefined) {

Drupal.behaviors.commonSchool = {
  attach: function(context, settings) {

    $.fn.countrySelect = function() {
      self = this;
      checkbox = this.find('.form-checkbox');

      init = function() {
        checkbox.on('change', function() {
          $('.country').toggle();

          if ($('.administrative-area select').attr('disabled')) {
            $('.administrative-area select').removeAttr('disabled');
            return false;
          }

          $('.administrative-area select').attr('disabled', true);
        });
      };

      return init();
    };

    $('.form-item-facets-outside').countrySelect();

    if ($('.field-date').length) {
      $('.field-date').datepicker();
    }
  }
};

})(jQuery, this);
;
