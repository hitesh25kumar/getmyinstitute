/**
 * @file interactive_common.js
 */

(function ($, Drupal, window, document) {

  Drupal.behaviors.common_ready = {
    attach: function(context, settings) {

      if (settings.common_ready && settings.common_ready.variables.use_ajax_pager) {
        // AJAX "View more" paging.
        Drupal.behaviors.common_ready.pagingViewMore(context, settings);
      }

      // Add submit behavior for keywords box.
      $('#common-counselor-facet-form #edit-keywords').on('change',function(){
        $(this).parents('#common-counselor-facet-form').submit();
      });

    },

    pagingViewMore: function(context, settings) {
      // Bind "View more" button click event.
      $('a.pager-view-more', context).click(function(evt) {
        evt.preventDefault();

        var self = $(this);

        // Prevent user from double-clicking while waiting on response below.
        if (self.hasClass('loading')) {
          return false;
        }

        self.addClass('loading');
        var page = self.data('page'),
            hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'),
            query_string = 'page=' + (page + 1);

        for(var i = 0; i < hashes.length; i++) {
          query_string = query_string+'&'+hashes[i];
        }
        var target = '/ajax/ready-resource?'+query_string;
        self.attr('rel', escape(self.html()));
        self.html('Loading More...');

        var xhr = $.get(target, {}, function(response) { });

        xhr.success(function(response) {
          // Stop early if we have nothing to load.
          if (!response.more) {
            self.remove();
          }

          // execute ajax request and deal with returned posts.
          // Add nodes to the page.
          var container = $('.interactive-fluid .entities');
          for (var i = 0; i < response.nodes.length; i++) {
            container.append(response.nodes[i].markup);
          }

          // Re-init masonry
          masonry();
          
          // Re-enable Video modal.
          modals();

          // Re-enable click.
          self.removeClass('loading');
          self.html(unescape(self.attr('rel')));
          self.data('page', page + 1);
          self.removeAttr('rel');          

        });
      });
    }
  };

})(jQuery, Drupal, this, this.document);
;
