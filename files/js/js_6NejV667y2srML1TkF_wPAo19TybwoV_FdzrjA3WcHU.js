(function ($, root, undefined) {

Drupal.behaviors.commonEvent = {
  attach: function(context, settings) {
    // First, set the dropdown's to submit().
    $(document).ready(function(){
      $('#common-event-event-keyword-search #edit-keywords,#common-event-event-keyword-search #edit-locations,#common-event-event-keyword-search #edit-countries').on('change',function(){
        $(this).parents('#common-event-event-keyword-search').submit();
      });
    });

    if (settings.common_event.dates) {
      //settings.common_event.subtypePath;
      //settings.common_event.dates;
      // Set defatulDate.
      var querystring = window.location.search.slice(1).split('&');
      var q = new Array;
      for (var i=0;i<querystring.length;i++) {
        var piece = querystring[i].split('=');
        q[piece[0]] = piece[1];
      }
      if ('date' in q) {
        var defaultDate = new Date(q['date']*1000);
      } else {
        var defaultDate = null;
      }

      // Now we need to enable the calendar.
      $('.event-calendar').datepicker({
        defaultDate: defaultDate,
        firstDay: 1,
        dayNamesMin: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
        showOtherMonths: true,
        selectOtherMonths: true,
        //minDate: 0,
        beforeShowDay: function(date) {
          var onoff = false;
          var theClass = '';
          var year = date.getFullYear();
          var month = date.getMonth()+1;
          var day = date.getDate();
          if (month<10) {
            month = '0'+month;
          }
          if (day<10) {
            day = '0'+day;
          }
          // We need to check each depth of the array.
          if (settings.common_event.dates[year]) {
            if (settings.common_event.dates[year][month]) {
              if (settings.common_event.dates[year][month][day]) {
                onoff = true;
                theClass = 'has-event';
              }
            }
          }

          return [onoff, theClass, ''];
        },
        onSelect: function(dateText, inst) {
          // We need to split the date text to get the keys for our dates array.
          var date = dateText.split('/');
          var location = '/'+settings.common_event.subtypePath+'?date='+settings.common_event.dates[date[2]][date[0]][date[1]];
          
          // We also need today's date, so that we add the archive query if
          // an expired event is clicked. But we need to format it to match.
          var today = new Date();
          var todayDay = today.getDate();
          if (todayDay<10) {
            todayDay = '0'+todayDay;
          }
          var todayMonth = today.getMonth()+1;
          if (todayMonth<10) {
            todayMonth = '0'+todayMonth;
          }          
          var todayText = todayMonth+'/'+todayDay+'/'+today.getFullYear();
          
          // Now check if the clicked date is in the past.
          if (dateText < todayText) {
            var location = location+'&archive=1';
          }
          
          // Last, redirect.
          window.location.href = location;
        }
      });
    }
  }
};

})(jQuery, this);
;
