(function ($) {
  Drupal.behaviors.interactiveComponent = {
    attach: function (context, settings) {

      // Make sure a Rotator exists so we do not get JS errors.
      if (settings.interactiveComponent !== undefined && settings.interactiveComponent.length >= 1) {
         $.each(settings.interactiveComponent, function(index, sliderConfig) {
          // Initiate Sudo Slider.
          var instanceSlider = $(sliderConfig.id).sudoSlider(sliderConfig.sudoslider);

          // Enquire Sizes.
          sizeEnquire({
            element: sliderConfig.id,
            sSettings: sliderConfig.sudoslider,
            eSettings: sliderConfig.enquire,
            sreenCount: null,
            slider: instanceSlider,
          });
        });
      }

    }
  };

  /**
   * Wrapper loop for register enquire.js funcitons.
   *
   * @param object sliderConfig.
   *   Configuration settings passed to js from PHP.
   *   See interactive_component_default_sudoslider().
   *
   * @return.
   */
  function sizeEnquire(sliderConfig) {
    if (!sliderConfig.eSettings) {
      return sliderConfig;
    }

    sliderConfig.sreenCount = sliderConfig.eSettings.length;
    for (var i = 0; i < sliderConfig.sreenCount; i++) {
      initEnquire(i, sliderConfig);
    }
  }

  /**
   * Set each individual enquire.js register.
   *
   * @param integer i.
   *   Numerical number which sets the configuration to use.
   * @param object sliderConfig.
   *   Configuration settings passed to js from PHP.
   *   See interactive_component_default_sudoslider().
   *
   * @return.
   */
  function initEnquire(i, sliderConfig) {
    enquire.register(sliderConfig.eSettings[i].media, {
      match : function() {

        var self = this;
        self.pClasses = $(sliderConfig.element).parents('.interactive-component-parent').attr('class');
        self.cArray = self.pClasses.split(' ');
        self.cCount = null;

        $(self.cArray).each(function(index, value) {
          if (value.indexOf('column-') !== 0) {
            return;
          }

          self.cCount = Number(value.split('-')[1]);
        });

        self.tSlides = sliderConfig.slider.getValue('totalSlides');
        self.eSlides = Number(sliderConfig.eSettings[i].slide);

        deinit = (self.cCount === self.tSlides && self.cCount === self.eSlides);
        if (deinit) {
          sliderConfig.slider.destroy();
          return false;
        }

        if (sliderConfig.slider.getValue('destroyed')) {
          sliderConfig.slider.init();
        }

        // Update the number of slides in a frame.
        sliderConfig.slider.setOption('slideCount', self.eSlides);
        sliderConfig.slider.setOption('moveCount', Number(sliderConfig.eSettings[i].move));
      },
    }, sliderConfig.eSettings[i].shouldDegrade);
  }

})(jQuery);
;
